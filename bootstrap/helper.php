<?php

if (! function_exists('thousand')) {

    /**
     * Convert number to thousand separated string.
     *
     * @param int $number
     * @param string $separator
     */
    function thousand($number, $separator = '.') {
        return number_format($number, 0, ',', $separator);
    }
}
