#Tonjoo Tes Junior dan Experience

## REQUIREMENTS
1. All laravel 5.8 requirement on the [documentation](https://laravel.com/docs/5.8/installation#installation)

## Installation
1. Install Dependency by running command `composer install`
2. Copy the `.env.example` into `.env` file
3. Generate app key by running php artisan key:generate
4. Set the environment variable of `.env` with your database environment
```
DB_CONNECTION=mysql
DB_DATABASE=tonjoo #OR YOUR DATABASE NAME
DB_USERNAME=root #OR YOUR LOCAL MYSQL USERNAME
DB_PASSWORD= #OR YOUR LOCAL MYSQL PASSWORD
```
5. Run the `php artisan serve` and access `http://localhost:8000`

## CATATAN URL
### JUNIOR
- Soal Junior HTML ada di `/junior/html` yang file nya berada di `resources/views/junior/html.html` asset ada pada folder `public/`
- Soal Junior PHP ada di `/junior/php` yang controller berada di `app/Http/Controllers/EmployeeController` dan views berada di `resources/views/junior/php/*.blade.php`
- Soal Junior JQUERY ada di `/junior/jquery` yang file nya berada di `resources/views/junior/jquery.html` asset ada pada folder `public/`

### EXPERIENCED
- Soal Master Data Repeatable ada di `/experienced/transactions` menggunakan controller `TransactionController` dan `TransactionDetailController` dan viewnya berada di `experienced/transactions`
- Soal Logika ada di `/logika` atau ada pada file `app/Services/TabunganService.php`

## DEPENDENCY
### HELPER
pada file `bootstrap/helper.php`

### COMPOSER
1. Laravel
2. therour/laravel-sb-admin-2 [github](https://github.com/therour/laravel-sb-admin-2) package template dashboard created by the author
