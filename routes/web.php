<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// JUNIOR
Route::prefix('junior')->name('junior.')->group(function () {
    // JUNIOR INDEX
    Route::get('/', function () {
        return '<ul>
        <li><a href="'.route('junior.html').'">HTML</a></li>
        <li><a href="'.route('junior.php.index').'">PHP</a></li>
        <li><a href="'.route('junior.jquery').'">JQUERY</a></li>
    </ul>';
    });
    // JUNIOR HTML
    Route::view('/html', 'junior.html')->name('html');
    // JUNIOR JQUERY
    Route::view('/jquery', 'junior.jquery')->name('jquery');
    // JUNIOR PHP
    Route::apiResource('php', 'EmployeeController')->parameters([
        'php' => 'employee'
    ]);
});

// EXPERIENCED
Route::prefix('experienced')->name('experienced.')->group(function () {
    Route::get('transactions/data', 'TransactionController@data')->name('transactions.data');
    Route::get('transactions/rekap', 'TransactionController@rekap')->name('transactions.rekap');
    Route::resource('transactions', 'TransactionController')->except(['show', 'destroy']);
    Route::delete('transactions/detail/{detail}', 'TransactionDetailController@destroy')
        ->name('transactions.details.destroy');
});

Route::view('logika', 'logika');
