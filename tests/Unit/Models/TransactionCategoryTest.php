<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Transaction;
use App\TransactionCategory;

class TransactionCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_many_transactions()
    {
        $transactions = factory(Transaction::class, 10)->create();

        // dd(TransactionCategory::first()->transactions()->toSql());
        dd(TransactionCategory::first()->transactions()->distinct('code')->get()->toArray());
    }
}
