<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use App\TransactionDetail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionDetailTest extends TestCase
{
    /** @test */
    public function it_can_join_transaction_query()
    {
        $query = TransactionDetail::joinTransaction();

        $this->assertStringContainsString(
            'join "transactions" on "transaction_details"."transaction_code" = "transactions"."code"',
            $query->toSql()
        );
    }

    /** @test */
    public function it_can_join_transaction_category_query()
    {
        $query = TransactionDetail::joinTransactionCategory();

        $this->assertStringContainsString(
            'join "transaction_categories" on "transaction_details"."transaction_category_id" = "transaction_categories"."id"',
            $query->toSql()
        );
    }
}
