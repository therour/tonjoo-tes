<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use App\Transaction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_many_details()
    {
        $transaction = factory(Transaction::class)->create();

        $this->assertInstanceOf(Collection::class, $transaction->details);
    }
}
