<?php

return [
    // set true to enable Demo routes : '/demos'
    'demo' => true,

    'topbar' => 'layouts.topbar', // default sb-admin-2::layouts.partials.topbar
    'sidebar-menu' => 'layouts.sidebar-menu', // default sb-admin-2::layouts.partials.sidebar-menu
    'brand' => 'layouts.brand', // default sb-admin-2::layouts.partial.brand

    'component' => [
        'enable' => false,
        'registers' => [
            'heading' => 'sb-admin-2::components.heading',
            'box'     => 'sb-admin-2::components.box',
        ]
    ],
];
