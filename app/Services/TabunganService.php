<?php

namespace App\Services;

class TabunganService
{
    /**
     * Jenis pecahan yang ada.
     *
     * @var array
     */
    protected $jenisPecahan = [100000, 50000, 20000, 5000, 100, 50];

    /**
     * Ambil uang.
     *
     * @param int $uang
     * @return array
     */
    public function ambilUang(int $uang)
    {
        $pecahan = [];
        foreach ($this->jenisPecahan as $jenis) {
            $pecahan[$jenis] = (int) ($uang / $jenis);
            $uang -= $pecahan[$jenis] * $jenis;
        }

        return $pecahan;
    }
}
