<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TransactionStoreRequest;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->getTransactionDataQuery($request)
            ->latest('payment_date')
            ->latest('created_at');

        $transactionDetails = $query->paginate($request->per_page ?? 10);

        return view('experienced.transactions.index', compact('transactionDetails'));
    }

    /**
     * Rekap Transaksi
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function rekap(Request $request)
    {
        $query = $this->getTransactionDataQuery($request)
            ->groupBy(['payment_date', 'transaction_category_id'])
            ->latest('payment_date')
            ->select([
                'payment_date',
                'transaction_categories.name as category_name',
                DB::raw('sum(transaction_details.detail_nominal) as sum_nominal'),
            ]);

        return view('experienced.transactions.rekap', [
            'transactionDetails' => $query->paginate($request->per_page ?? 10)
        ]);
    }

    /**
     * List data transaksi
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        $query = $this->getTransactionDataQuery($request);

        $transactionDetails = $query->paginate($request->per_page ?? 10);

        return view('experienced.transactions.data', compact('transactionDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('experienced.transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            /** @var \App\Transaction $transaction */
            $transaction = Transaction::create($request->all());
            $detailsData = [];
            foreach ($request->details['name'] as $i => $detail_name) {
                $detailsData[] = [
                    'detail_name' => $detail_name,
                    'detail_nominal' => $request->details['nominal'][$i],
                    'transaction_category_id' => $request->details['category_id'][$i],
                ];
            }
            $transaction->details()->createMany($detailsData);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        return redirect()->route('experienced.transactions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        return view('experienced.transactions.edit', compact('transaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionStoreRequest $request, Transaction $transaction)
    {
        $transaction->update($request->validated());

        return redirect()->route('experienced.transactions.index');
    }

    /**
     * Build the transaction data query.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getTransactionDataQuery(Request $request)
    {
        return TransactionDetail::query()
            ->select(['transaction_details.*'])
            ->joinTransaction()
            ->joinTransactionCategory()
            ->when($search = $request->query('search'), function ($query) use ($search) {
                $query->where(function ($q) use ($search) {
                    $q->where('description', 'LIKE', '%'.$search.'%');
                    $q->orWhere('detail_name', 'LIKE', '%'.$search.'%');
                });
            })
            ->when($startDate = $request->query('start_date'), function ($query) use ($startDate) {
                $startDate = Carbon::createFromFormat('d/m/Y', $startDate);
                $query->whereDate('payment_date', '>=', $startDate);
            })
            ->when($endDate = $request->query('end_date'), function ($query) use ($endDate) {
                $endDate = Carbon::createFromFormat('d/m/Y', $endDate);
                $query->whereDate('payment_date', '<=', $endDate);
            })
            ->when($category = $request->query('category'), function ($query) use ($category) {
                $query->where('transaction_category_id', $category);
            });
    }
}
