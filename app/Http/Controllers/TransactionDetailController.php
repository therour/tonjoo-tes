<?php

namespace App\Http\Controllers;

use App\TransactionDetail;

class TransactionDetailController extends Controller
{
    /**
     * Destoy transaction detail.
     *
     * @param \App\TransactionDetail $detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionDetail $detail)
    {
        $detail->delete();

        return redirect()->back();
    }
}
