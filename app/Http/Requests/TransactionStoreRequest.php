<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required',
            'euro_rate' => 'required|numeric|min:0',
            'payment_date' => 'required|date',
            'description' => 'required',
            'details.name' => 'array|min:1',
            'details.nominal.*' => 'numeric|min:1',
        ];
    }
}
