<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCategory extends Model
{
    /**
     * has many transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function transactions()
    {
        return $this->hasManyThrough(
            Transaction::class,
            TransactionDetail::class,
            'transaction_category_id',
            'code',
            'id',
            'transaction_code'
        );
    }

    /**
     * has many detail transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailTransactions()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_category_id');
    }
}
