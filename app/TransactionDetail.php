<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EloquentBuilders\TransactionDetailBuilder;

/**
 * @mixin \App\EloquentBuilders\TransactionDetailBuilder
 */
class TransactionDetail extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['detail_name', 'detail_nominal', 'transaction_category_id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'nominal' => 'integer',
        'transaction_category_id' => 'integer',
    ];

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = ['transaction'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            $transaction = $model->transaction;
            dump($transaction->details()->count());
            if ($transaction->details()->count() == 0) {
                $transaction->delete();
            }
        });
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new TransactionDetailBuilder($query);
    }

    /**
     * belongs to transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * Belongs to category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(TransactionCategory::class, 'transaction_category_id');
    }
}
