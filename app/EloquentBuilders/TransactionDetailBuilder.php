<?php

namespace App\EloquentBuilders;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

/**
 * @mixin \Illuminate\Database\Query\Builder
 */
class TransactionDetailBuilder extends EloquentBuilder
{
    /**
     * Prepend table name to each column name.
     *
     * @param string $table
     * @param array $columns
     * @return array
     */
    private function prependTableNameToColumns(string $table, array $columns)
    {
        return array_map(function ($column) use ($table) {
            return $table . '.' . $column;
        }, $columns);
    }

    /**
     * Join to Transaction table.
     *
     * @param array $select
     * @return \App\EloquentBuilders\TransactionDetailBuilder
     */
    public function joinTransaction($select = array('description', 'code', 'euro_rate', 'payment_date'))
    {
        return $this->join('transactions', 'transaction_details.transaction_code', '=', 'transactions.code')
            ->addSelect($this->prependTableNameToColumns('transactions', $select));
    }

    /**
     * Join to Transaction category table.
     *
     * @param array $select
     * @return \App\EloquentBuilders\TransactionDetailBuilder
     */
    public function joinTransactionCategory($select = array('name as category_name'))
    {
        $query = $this->join(
            'transaction_categories',
            'transaction_details.transaction_category_id',
            '=',
            'transaction_categories.id'
        );

        $query = $query->addSelect($this->prependTableNameToColumns('transaction_categories', $select));

        return $query;
    }
}
