<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Transaction;
use Faker\Generator as Faker;
use App\TransactionDetail;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->numerify('######'),
        'euro_rate' => $faker->numberBetween(14000, 16000),
        'payment_date' => now(),
        'description' => $faker->sentence,
    ];
});

$factory->afterCreating(Transaction::class, function (Transaction $transaction, Faker $faker) {
    $transaction->details()->saveMany(
        factory(TransactionDetail::class, $faker->numberBetween(1, 5))->make([
            'transaction_code' => $transaction->code,
        ])
    );
});
