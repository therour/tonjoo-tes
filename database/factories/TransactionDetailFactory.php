<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\TransactionDetail;
use Faker\Generator as Faker;
use App\Transaction;
use App\TransactionCategory;

$factory->define(TransactionDetail::class, function (Faker $faker) {
    return [
        'transaction_code' => function () {
            return factory(Transaction::class)->create();
        },
        'transaction_category_id' => function () {
            return TransactionCategory::inRandomOrder()->first();
        },
        'detail_name' => $faker->realText(20),
        'detail_nominal' => $faker->numberBetween(10000, 30000),
    ];
});
