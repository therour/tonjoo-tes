@extends('junior.php.layout')

@section('content')
<div class="container">
    <div class="container" id="app" style="display: none;">
        <button class="btn btn-primary text-uppercase border-0 shadow-sm"
            v-show="!createFormOpened"
            v-on:click="toggleCreateForm">
            Add new employee
        </button>
        <div v-show="createFormOpened" style="display:none;">
            <div class="card card-body">
                <h2 class="mb-3">Add new employee</h2>
                <div class="form-group">
                    <label for="name">{{ __('name') }}</label>
                    <input type="text" class="form-control" v-model="newEmployee.name" id="name" placeholder="John Doe">
                </div>
                <div class="form-group">
                    <label for="age">{{ __('age') }}</label>
                    <input type="number" class="form-control" v-model="newEmployee.age" id="age" placeholder="25 years">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" v-on:click="submitCreateForm">Create Employee</button>
                    <button class="btn btn-link" v-on:click="toggleCreateForm">Cancel</button>
                </div>
            </div>
        </div>
        <div class="card card-body p-0 mt-3">
            <table class="table table-hover table-striped mb-0" v-if="employees.length > 0">
                <thead>
                    <tr class="text-muted text-sm">
                        <th class="border-top-0"><small class="font-weight-bold text-uppercase">Employee</small></th>
                        <th class="border-top-0"><span class="sr-only">Actions</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="employee in employees">
                        <td>
                            <div v-if="!employee.isEditing">
                                <span class="d-block">@{{ employee.name }}</span>
                                <small class="text-muted">@{{ employee.age }} years old</small>
                            </div>
                            <div v-else>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label class="mr-2">{{ __('name') }}</label>
                                        <input type="text" class="form-control mr-2" v-model="employee.name">
                                        <label class="mr-2">{{ __('age') }}</label>
                                        <input type="number" class="form-control" v-model="employee.age">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="align-middle text-right">
                            <button class="btn shadow-sm btn-primary" v-if="employee.isEditing" v-on:click="submitChange(employee)">Change</button>
                            <button class="btn btn-link" v-if="employee.isEditing" v-on:click="employee.isEditing = false">Cancel</button>
                            <button class="btn shadow-sm btn-warning" v-if="!employee.isEditing" v-on:click="employee.isEditing = true">Edit</button>
                            <button class="btn shadow-sm btn-danger" v-on:click="deleteEmployee(employee)">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="my-4 text-center" v-if="employees.length == 0">
                <h3 class="text-muted"><strong>Opps!</strong> it's look like you are trying hard alone.</h3>
                <button class="btn btn-link" v-on:click="createFormOpened = true;"><h3><small>Go recruit some employee !</small></h3></button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('js/vue-axios.js') }}"></script>
<script>
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;

    var app = new Vue({
        el: '#app',
        data: function () {
            return {
                createFormOpened: false,
                employees: [],
                newEmployee: {
                    name: '',
                    age: null
                }
            };
        },
        methods: {
            submitChange: function (employee) {
                axios.put('{{ route('junior.php.index') }}/' + employee.id, {
                    name: employee.name,
                    age: employee.age,
                }).then(function (response) {
                    Object.assign(employee, response.data);
                    employee.isEditing = false;
                })
            },
            deleteEmployee: function (employee) {
                if (! confirm('Are you sure to delete ' + employee.name)) {
                    return;
                }
                axios.delete('{{ route('junior.php.index') }}/' + employee.id)
                    .then(function (response) {
                        app.employees.splice(
                            app.employees.findIndex(function (obj) {
                                return obj.id = employee.id;
                            })
                        , 1)
                    })
            },
            toggleCreateForm: function (e) {
                this.createFormOpened = ! this.createFormOpened;
                if (this.createFormOpened) {
                    setTimeout(function () {
                        document.getElementById('name').focus();
                    }, 100)
                }
            },
            submitCreateForm: function (e) {
                axios.post("{{ route('junior.php.index') }}", this.newEmployee)
                .then(function (response) {
                    console.log(response);
                    var newEmployee = response.data;
                    newEmployee.isEditing = false;
                    app.employees.push(newEmployee);
                    }).catch(function (err) {
                        console.log(err.response);
                        alert(err.response.data.message);
                    }).then(function () {
                        app.newEmployee.name = '';
                        app.newEmployee.age = null;
                    })
            }
        },
        mounted: function () {
            axios.get("{{ route('junior.php.index') }}")
                .then(function (response) {
                    var employees = response.data.data;
                    app.employees = employees.map(function (employee) {
                        employee.isEditing = false;
                        return employee;
                    });
                })
            document.getElementById('app').style.display = 'block';
        }
    });
</script>
@endpush
