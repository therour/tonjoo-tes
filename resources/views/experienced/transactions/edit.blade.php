@extends('layouts.app')
@setOption('title', 'Edit Data Transaksi')

@push('css')
<style>
.detail-transaction-table {
    border-left: 1px solid #e3e6f0;
}
.detail-transaction-table td, .detail-transaction-table th {
    border: 1px solid #e3e6f0;
}
.detail-transaction-table tr td:last-child,
.detail-transaction-table tr th:last-child {
    border-right: 0px;
    border-bottom: 0px;
    border-top: 0px;
}
</style>
@endpush

@section('content')
    <div class="card card-body shadow">
        <form action="{{ route('experienced.transactions.update', $transaction->code) }}" method="POST" id="createTransactionForm">
            @csrf @method('PUT')
            <div class="form-row flex-md-row-reverse justify-content-end">
                <div class="col-md-6">
                    {{-- Code --}}
                    <div class="form-group row">
                        <label for="code" class="col-sm-4 col-form-label text-md-right">Code</label>
                        <div class="col-sm-8">
                            <input id="code" name="code" type="text" class="form-control @error('code') is-invalid @enderror" value="{{ old('code', $transaction->code) }}">
                            @error('code')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    {{-- Euro Rate --}}
                    <div class="form-group row">
                        <label for="euroRate" class="col-sm-4 col-form-label text-md-right">Kurs Euro</label>
                        <div class="col-sm-8">
                            <input id="euroRate" name="euro_rate" type="number" class="form-control @error('euro_rate') is-invalid @enderror" value="{{ old('euro_rate', $transaction->euro_rate) }}">
                            @error('euro_rate')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    {{-- Payment Date --}}
                    <div class="form-group row">
                        <label for="paymentDate" class="col-sm-4 col-form-label text-md-right">Tanggal Bayar</label>
                        <div class="col-sm-8">
                            <input id="paymentDate" name="payment_date" type="date" class="form-control @error('payment_date') is-invalid @enderror" value="{{ old('payment_date', $transaction->payment_date->format('Y-m-d')) }}">
                            @error('payment_date')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="description" class="col-12 col-lg-3 col-form-label">Deskripsi</label>
                        <div class="col-12 col-lg-9">
                            <textarea name="description" id="description" cols="2" class="form-control">{{ old('description', $transaction->description) }}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <h3>Data Transaksi</h3>
            <div class="container" id="transactionDetails">
                @foreach ($transaction->details->groupBy->transaction_category_id as $category_id => $details)
                <div class="card mb-3">
                    <div class="card-header d-flex justify-content-between">
                        <div class="form-inline flex-grow-1">
                            <label for="">Kategori</label>
                            <select type="text" class="category form-control ml-2 w-50">
                                <option value="1" {{ $category_id == 1 ? 'selected' : '' }}>Pemasukan</option>
                                <option value="2" {{ $category_id == 2 ? 'selected' : '' }}>Pengeluaran</option>
                            </select>
                        </div>
                        <button type="button" class="close text-danger">&times;</button>
                    </div>
                    <div class="card-body">
                        <table class="table detail-transaction-table mb-0">
                            <thead><tr>
                                <th>Nama Transaksi</th>
                                <th>Nominal (Rupiah)</th>
                                <th></th>
                            </tr></thead>
                            <tbody>
                                @foreach ($details as $detail)
                                <tr>
                                    <td><input type="text" name="details[name][]" value="{{ $detail->detail_name }}" class="form-control d-none">{{ $detail->detail_name }}</td>
                                    <td><input type="text" name="details[nominal][]" value="{{ $detail->detail_nominal }}" class="form-control d-none">{{ thousand($detail->detail_nominal) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger rounded-circle border-0 remove-detail-transaction-button">
                                            <i class="fas fa-sm fa-minus"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                                <tr class="add-detail-transaction-form">
                                    <td><input type="text" class="form-control form-control-sm" placeholder="nama transaksi"></td>
                                    <td><input type="number" class="form-control form-control-sm" placeholder="Rp ###.###"></td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-success rounded-circle border-0 add-detail-transaction-button">
                                            <i class="fas fa-sm fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
                <div class="text-right">
                    <button id="addCategoryBtn" type="button" class="btn btn-info">Tambah Kategori</button>
                </div>
            </div>
            <div class="container-fluid text-center mt-4">
                <button class="btn btn-lg btn-primary border-0 shadow px-4">Simpan</button>
                <button class="btn btn-lg btn-link text-dark">Batal</button>
            </div>
        </form>
    </div>
@endsection

@push('script')
<script>
function parseThousand(number) {
    var parts = number.toString().split(",");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");

    return parts.join(",");
}

$(document).ready(function () {
    $('#transactionDetails input').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
    var removeDetailTransactionHandler = function (e) {
        $(e.target).closest('tr').remove();
    };
    $('.remove-detail-transaction-button').click(removeDetailTransactionHandler);
    $('.add-detail-transaction-button').click(function () {
        var $tr = $(this).closest('tr');
        var $input = $tr.find('input');
        var name = $input[0].value;
        var nominal = $input[1].value;
        if (name && nominal) {
            var newRow = '<tr>' +
                '<td><input type="text" class="d-none" name="details[name][]" value="'+name+'" readonly>'+name+'</td>' +
                '<td><input type="text" class="d-none" name="details[nominal][]" value="'+nominal+'"readonly>'+parseThousand(nominal)+'</td>' +
                '<td><button type="button" class="btn btn-sm btn-danger rounded-circle border-0 remove-detail-transaction-button">' +
                '<i class="fas fa-sm fa-minus"></i>' +
                '</button></td>' +
                '</tr>';
            $(newRow).insertBefore($tr)
                .find('.remove-detail-transaction-button').click(removeDetailTransactionHandler);
            $input.val('');
            $input[0].focus();
        } else {
            if (!$input.hasClass('is-invalid')) {
                $input.each(function (i, input) {
                    if (! input.value) {
                        $(input).addClass('is-invalid');
                    }
                });
                setTimeout(function () {
                    $input.removeClass('is-invalid');
                }, 1000);
            }
        }
    });
    $('#transactionDetails .card .card-header .close').click(function (e) {
        $(e.target).closest('.card').remove();
    });
    var $categoryCardStub = $('#transactionDetails .card').first().clone(true, true);
    $('#addCategoryBtn').click(function () {
        var $categoryCard = $categoryCardStub.clone(true, true);
        $categoryCard.find('table > tbody > tr:not(.add-detail-transaction-form)').remove();

        $categoryCard.insertBefore(this);
    });
    $('#createTransactionForm').submit(function (e) {
        e.preventDefault();
        $('#transactionDetails > .card > .card-body > .table > tbody > tr:not(:last-child)').each(function (i, row) {
            var val = $(row).closest('.card').find('option:selected').val();
            $(row).prepend(
                $('<input type="hidden" name="details[category_id][]" value="">').val(val)
            )
        });
        return e.target.submit();
    })
});
</script>
@endpush
