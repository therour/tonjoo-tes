@extends('layouts.app')
@setOption('title', 'Data Transaksi')

@push('css')
<link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="d-flex">
        <form action="{{ request()->fullUrl() }}" class="form-inline ml-auto">
            <input type="text" class="form-control mr-2 datepicker-here" data-language="id" name="start_date" value="{{ request('start_date') }}" placeholder="tanggal">
            <span class="mr-2">sampai</span>
            <input type="text" class="form-control mr-2 datepicker-here" data-language="id" name="end_date" value="{{ request('end_date') }}" placeholder="tanggal">
            <select id="selectCategory" class="form-control mr-2" name="category">
                <option value="">-- Semua Kategori --</option>
                @foreach (\App\TransactionCategory::all() as $category)
                <option value="{{ $category->id }}"{{ request('category') == $category->id ? ' selected' : '' }}>{{ $category->name }}</option>
                @endforeach
            </select>
            <input type="text" class="form-control mr-2" placeholder="Search" name="search" value="{{ request('search') }}">
            <button type="submit" class="btn btn-outline-primary">
                Search
            </button>
        </form>
    </div>
    <div class="card card-body shadow mt-3 p-0">
        <table class="table mb-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Deskripsi</th>
                    <th>Code</th>
                    <th>Rate Euro</th>
                    <th>Payment Date</th>
                    <th>Kategori</th>
                    <th>Nama Transaksi</th>
                    <th>Nominal (IDR)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transactionDetails as $i => $data)
                <tr>
                    <td>{{ $transactionDetails->firstItem() + $i }}</td>
                    <td>{{ $data->description }}</td>
                    <td>{{ $data->code }}</td>
                    <td>{{ thousand($data->euro_rate) }}</td>
                    <td>{{ Carbon\Carbon::parse($data->payment_date)->format('d F Y') }}</td>
                    <td>{{ $data->category_name }}</td>
                    <td>{{ $data->detail_name }}</td>
                    <td>{{ thousand($data->detail_nominal) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="justify-content-between d-flex mt-3">
        <div>
            <div class="form-group form-inline">
                <form action="{{ request()->fullUrl() }}">
                    <select name="per_page" class="form-control mr-2" style="min-width: 60px;"
                    onchange="this.form.submit()"
                    >
                        <option value="10" {{ request('per_page', 10) == 10 ? 'selected' : '' }}>10</option>
                        <option value="25" {{ request('per_page', 10) == 25 ? 'selected' : '' }}>25</option>
                        <option value="50" {{ request('per_page', 10) == 50 ? 'selected' : '' }}>50</option>
                    </select>
                    <span>Menampilkan {{ $transactionDetails->count() }} dari {{ $transactionDetails->total() }}</span>
                </form>
            </div>
        </div>
        {{ $transactionDetails->appends(request()->query())->links() }}
    </div>
@endsection

@push('script')
<script src="{{ asset('/js/datepicker.min.js') }}"></script>
<script src="{{ asset('/js/datepicker.id.js') }}"></script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endpush
