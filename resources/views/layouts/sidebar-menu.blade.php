@sidebarMenu([
    'title' => 'Dashboard',
    'icon' => 'fas fa-fw fa-tachometer-alt',
    'url' => '#',
])

@sidebarDropdown([
    'title' => 'Data Transaksi',
    'icon' => 'fas fa-fw fa-coins',
    'active' => 'experienced/transactions*'
], function ($dropdown) {
    $dropdown->menu(['title' => 'Data Transaksi', 'route' => 'experienced.transactions.data']);
    $dropdown->menu(['title' => 'Tambah Data Transaksi', 'route' => 'experienced.transactions.create']);
    $dropdown->menu(['title' => 'List Data Transaksi', 'route' => 'experienced.transactions.index']);
    $dropdown->menu(['title' => 'Rekap Data Transaksi', 'route' => 'experienced.transactions.rekap']);
})

@sidebarDivider
{{--
@sidebarHeading('Example')

@sidebarDropdown([
    'title' => 'Dropdowns',
    'icon' => 'fas fa-fw fa-cog',
    ], function ($dropdown) {
        $dropdown->heading('Dropdown Menus:');
        $dropdown->menu(['title' => 'Menu 1', 'url' => '#']);
        $dropdown->menu(['title' => 'Menu 2', 'url' => '#']);
    }
) --}}
